# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: Update Variable names in pipe YAML

## 1.0.0

- major: Update with production URLs and variable fixes

## 0.6.1

- patch: Update tests to use repository variables

## 0.6.0

- minor: Update readme with support details
- patch: Update readme with support details

## 0.5.5

- patch: Update readme with support details

## 0.5.4

- patch: Update repo url to beaglesecurity-test

## 0.5.3

- patch: Update Readme
- patch: Update pipe path to bitbucket repo
- patch: Update repo url in pipe.yml

## 0.5.2

- patch: Update Readme

## 0.5.1

- patch: Fix message formatting

## 0.5.0

- minor: Fix message formatting

## 0.4.0

- minor: Fix variable handling

## 0.3.0

- minor: Fix token error

## 0.2.0

- minor: Fix token leakage by making it as a repository variable

## 0.1.0

- minor: Initial release

